import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('../components/Login.vue')
  },
  {
    path: '/login',
    name: 'create',
    component: () => import('../components/create.vue')
  },
  {
    path: '/welcome',
    name: 'welcome',
    component: () => import('../components/Welcome.vue')
  },
  {
    path: '/admin',
    name: 'admin',
    component: () => import('../components/admin.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

